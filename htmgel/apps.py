# -*- coding: utf-8 -*-

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DefaultConfig(AppConfig):
    name = "htmgel"
    label = "htmgel"
    verbose_name = _("HTML (Custom)")


class Bootstrap4Config(AppConfig):
    name = "htmgel"
    label = "htmgel"
    verbose_name = _("HTML (Bootstrap 4)")


class UIKit3Config(AppConfig):
    name = "htmgel"
    label = "htmgel"
    verbose_name = _("HTML (UI Kit 3)")

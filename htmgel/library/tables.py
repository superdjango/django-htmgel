# Imports

from django.utils.safestring import mark_safe
from .base import BaseHTML

# Exports

__all__ = (
    "Column",
    "Row",
    "Table",
)

# Classes


class Column(BaseHTML):
    """A column (header) in an HTML table."""

    def __init__(self, name, label=None, **kwargs):
        """Initialize the column.

        :param name: The programmatic name of the column. For example, the field name.
        :type name: str

        :param label: The column label. If omitted, the name is converted into a label.
        :type label: str

        """
        content = label or name.replace("_", " ").title()
        super().__init__(content, open_tag="th", **kwargs)

        self.name = name

    @property
    def label(self):
        """Get the column label. Alias for ``content``."""
        return self.content

    @property
    def title(self):
        """Get the column label. Alias for ``content``."""
        return self.content


class Row(BaseHTML):
    """A row in an HTML table."""

    def __init__(self, data, **kwargs):
        """Initialize the row.

        :param data: The data to be included in the row.
        :type data: list | tuple

        """
        super().__init__("", open_tag="tr", **kwargs)

        self._data = data

    @property
    def data(self):
        """Alias for ``_data``."""
        return self._data

    @mark_safe
    def to_html(self):
        """Export the row to HTML."""
        a = list()
        a.append("<%s>" % self.get_open_tag())
        for d in self._data:
            a.append("<td>%s</td>" % d)

        a.append("</%s>" % self.get_close_tag())

        return "\n".join(a)


class Table(BaseHTML):
    """An HTML table."""

    def __init__(self, caption=None, columns=None, rows=None, **kwargs):
        """Initialize the table.

        :param caption: Optional caption for the table.
        :type caption: str

        :param columns: The columns to be included in the table.
        :type columns: list[Column]

        :param rows: The rows to be included in the table.
        :type rows: list[Row]

        """
        super().__init__("", open_tag="table", **kwargs)
        self.caption = caption
        self.columns = columns or list()
        self.rows = rows or list()

    def __iter__(self):
        return iter(self.rows)

    def add_column(self, name, label=None, **kwargs):
        """Add a column to the table.

        :param name: The programmatic name of the column. For example, the field name.
        :type name: str

        :param label: The column label. If omitted, the name is converted into a label.
        :type label: str

        :rtype: Column

        """
        column = Column(name, label=label, **kwargs)
        self.columns.append(column)

        return column

    def add_row(self, data, **kwargs):
        """Add a row to the table.

        :param data: The data to be included in the row.
        :type data: list | tuple

        :rtype: Row

        """
        row = Row(data, **kwargs)
        self.rows.append(row)

        return row

    @mark_safe
    def to_html(self):
        """Export the table as HTML."""
        a = list()
        a.append("<%s>" % self.get_open_tag())

        if self.caption is not None:
            caption = BaseHTML(self.caption, open_tag="caption")
            a.append(caption.to_html())

        a.append("<thead><tr>")
        for column in self.columns:
            a.append(column.to_html())
        a.append("</tr></thead>")

        a.append("<tbody>")

        for row in self.rows:
            a.append(row.to_html())

        a.append("</tbody>")

        a.append("</%s>" % self.get_close_tag())

        return "\n".join(a)

# Imports

from .base import BaseHTML

# Exports

__all__ = (
    "Anchor",
    "Link",
)

# Classes


class Anchor(BaseHTML):
    """An anchor."""

    def __init__(self, identifier, text=None, **kwargs):
        content = text or identifier
        kwargs['href'] = "#%s" % identifier
        kwargs['open_tag'] = "a"

        super().__init__(content, **kwargs)


class Link(BaseHTML):
    """An HTML link."""

    def __init__(self, href, text=None, **kwargs):
        content = text or href
        kwargs['href'] = href
        kwargs['open_tag'] = "a"

        super().__init__(content, **kwargs)

    @property
    def url(self):
        """Alias for ``href``."""
        return self.href

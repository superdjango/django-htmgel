# Imports

from django.conf import settings
# noinspection PyProtectedMember
from django.template import TemplateDoesNotExist
from django.utils.safestring import mark_safe
from ..shortcuts import parse_template
from .base import BaseHTML

# Exports

__all__ = (
    "Collection",
    "Script",
)

# Constants

STATIC_URL = settings.STATIC_URL

# Classes


class Collection(object):
    """A collection of JavaScript elements, either from source URLS or script  content.

    .. code-block:: python

        js = Collection()
        js.append("datepicker", path="bundled/datepicker/datepicker.min.js")
        js.append("datepicker-data-toggle", content="$('[data-toggle=\"datepicker\"]').datepicker();")

    """

    def __init__(self):
        self._identifiers = dict()
        self._items = list()

    def __iter__(self):
        return iter(self._items)

    def append(self, identifier, content=None, silent=True, url=None, wrap=True, **kwargs):
        """Add content or URL to media output.

        :param identifier: A unique identifier for the script.
        :type identifier: str

        :param content: The content to be added. Enclosing tags are optional.
        :type content: str

        :param silent: When ``False`` raise an exception rather than return ``False``.
        :type silent: bool

        :param url: The URL of the static file to include. This may also be given as an absolute URL.
        :type url: str

        :param wrap: Indicates the ``content`` should be wrapped in the appropriate ``<script>`` HTML tag.
        :type wrap: bool

        :rtype: bool
        :returns: ``True`` if successful.

        Additional keyword arguments are passed to the script instance.

        This method fails when:

        - The identifier simply ensures that requests to add the same media are not duplicated.
        - Both ``content`` and ``path`` have not been provided.

        """
        if identifier in self._identifiers:
            if silent:
                return False

            raise RuntimeWarning("Duplicate media identifier provided: %s" % identifier)

        if content is None and url is None:
            if silent:
                return False

            raise RuntimeWarning("The ``content`` or ``path`` argument is required.")

        if url is not None:
            if url.startswith("http") or url.startswith("/"):
                src = url
            else:
                src = "%s%s" % (STATIC_URL, url)
            
            script = Script(src=src, **kwargs)
        else:
            script = Script(content=content, wrap=wrap, **kwargs)
            
        self._items.append(script)

        index = len(self._items) - 1
        self._identifiers[identifier] = index

        return True

    @property
    def exists(self):
        """Indicates whether any scripts have been defined.

        :rtype: bool

        """
        return len(self._items) > 0

    def from_template(self, identifier, path, context=None, silent=True):
        """Add media from a template.

        :param identifier: A unique identifier for the media.
        :type identifier: str

        :param path: The path of the template file.
        :type path: str

        :param context: The context to use when parsing the template.
        :type context: dict

        :param silent: When ``False`` raise an exception rather than return ``False``.
        :type silent: bool

        :rtype: bool

        See notes on ``append()``.

        """
        try:
            content = parse_template(path, context or dict())
        except TemplateDoesNotExist as e:
            if silent:
                return False

            raise RuntimeWarning("Failed to load media from template: %s" % e)

        return self.append(identifier, content=content, silent=silent)

    def update(self, collection):
        """Update the current instance from another instance.

        :param collection: The other instance.
        :type collection: Type[Collection]

        .. tip::
            At runtime, it is safe for ``collection`` to be ``None``.

        """
        if collection is None:
            return

        # noinspection PyProtectedMember
        for identifier in collection._identifiers.keys():
            if identifier in self._identifiers:
                return

        # noinspection PyProtectedMember
        for line in collection._items:
            self._items.append(line)


class Script(BaseHTML):
    """Support for programmatically adding JavaScript to output.


    """
    
    def __init__(self, content=None, identifier=None, src=None, wrap=True, **kwargs):
        kwargs['open_tag'] = "script"
        super().__init__(content, **kwargs)

        self.identifier = identifier
        self.wrap = wrap

        if src is not None:
            self._attributes['src'] = src

    def get_close_tag(self):
        """The close tag is omitted when the content already includes the script tag."""
        if "src" in self._attributes or self.wrap:
            return super().get_close_tag()

        return ""

    def get_content(self):
        """Get script content only if ``src`` has not been provided."""
        if "src" in self._attributes:
            return ""

        return self.content

    def get_open_tag(self):
        """The open tag is omitted when the content already includes the script tag."""
        if "src" in self._attributes or self.wrap:
            return super().get_open_tag()

        return ""

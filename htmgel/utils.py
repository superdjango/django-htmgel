# Imports

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from .constants import BOOTSTRAP4, FOUNDATION6, UIKIT3

# Exports

__all__ = (
    "get_html_framework",
    "get_input_type",
    "is_required_field",
    "HTMLFramework",
)

# Functions


def get_html_framework():
    """Get the UI template prefix for generic templates.

    :rtype: str

    :raise: ImproperlyConfigured
    :raises: ``ImproperlyConfigured`` if a valid configuration has not been enabbled.

    """
    if "htmgel.apps.Bootstrap4Config" in settings.INSTALLED_APPS:
        return BOOTSTRAP4
    elif "htmgel.apps.Foundation6Config" in settings.INSTALLED_APPS:
        return FOUNDATION6
    elif "htmgel.apps.UIKit3Config" in settings.INSTALLED_APPS:
        return UIKIT3
    else:
        raise ImproperlyConfigured("An htmgel app config must be specified.")


def get_input_type(instance):
    """Get the type of field for a given instance.

    :param instance: The field instance to be checked.
    :type instance: Type[Field]

    :rtype: str

    """
    return instance.field.widget.__class__.__name__


def is_required_field(instance):
    """Determine if the given field is required.

    :param instance: The field instance to be checked.
    :type instance: Type[Field]

    :rtype: bool

    """
    return instance.field.required


# Classes


class HTMLFramework(object):

    @staticmethod
    def is_bootstrap():
        return "htmgel.apps.Bootstrap4Config" in settings.INSTALLED_APPS

    @staticmethod
    def is_foundation():
        return "htmgel.apps.Foundation6Config" in settings.INSTALLED_APPS

    @staticmethod
    def is_uikit():
        return "htmgel.apps.UIKit3Config" in settings.INSTALLED_APPS

# HTMGel

![](https://img.shields.io/badge/status-active-green.svg)
![](https://img.shields.io/badge/stage-development-blue.svg)
![](https://img.shields.io/badge/coverage-0%25-red.svg)

An app for more easily working with HTML elements in a Django project.

## Installation

Install the app:

```bash
pip install git+https://bitbucket.org/superdjango/django-htmgel/get/master.tar.gz;
```

## Settings

Add the appropriate app config to ``INSTALLED_APPS``. For example, to use Twitter Bootstrap 4:

```python
INSTALLED_APPS = [
    # ...
    'htmgel.apps.Bootstrap4Config',
    # ...
]
```

- ``Bootstrap3Config``: REMOVED.
- ``Bootstrap4Config``: Twitter Bootstrap 4
- ``Foundation6Config``: Zurb Foundation 6 - NOT IMPLEMENTED
- ``UIKit3Config``: UIKit 3

## Documentation

To generate the documentation, you need to ...

Install the additional requirements:

	pip install -r requirements.pip;
	
Create a demo project:

	django-admin startproject demo;
	
Run the make:

	(cd docs && make html);

Open ``docs/build/index.html``.

## History

For those interested in a long read on the theories behind HTMGel, we have prepared a
[history](docs/source/history.rst) of the prototyping and decisions that have led to the 
current version of the app.

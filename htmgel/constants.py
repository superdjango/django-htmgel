BOOTSTRAP4 = "bootstrap4"

BOOTSTRAP4_CDN_CSS = {
    'integrity': "sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS",
    'url': "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
}

BOOTSTRAP4_CDN_JS = {
    'integrity': "sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k",
    'url': "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js",
}

BOOTSWATCH_URL = "https://stackpath.bootstrapcdn.com/bootswatch/4.2.1/%(theme)s/bootstrap.min.css"

BOOTSWATCH_THEMES = {
    'cerulean': "sha384-62+JPIF7fVYAPS4itRiqKa7VU321chxfKZRtkSY0tGoTwcUItAFEH/HGTpvDH6e6",
    'cosmo': "sha384-BmPRQ4EzBa9ifYo2LjomrSZ28x2GHvKNtv599SfxGvi39OQfRzluOw+sLUfxPOOD",
}

FOUNDATION6 = "foundation6"

FOUNDATION6_CDN_CSS = {
    'integrity': "sha256-1mcRjtAxlSjp6XJBgrBeeCORfBp/ppyX4tsvpQVCcpA= sha384-b5S5X654rX3Wo6z5/hnQ4GBmKuIJKMPwrJXn52ypjztlnDK2w9+9hSMBz/asy9Gw sha512-M1VveR2JGzpgWHb0elGqPTltHK3xbvu3Brgjfg4cg5ZNtyyApxw/45yHYsZ/rCVbfoO5MSZxB241wWq642jLtA==",
    'url': "https://cdn.jsdelivr.net/npm/foundation-sites@6.5.1/dist/css/foundation.min.css",
}

FOUNDATION6_CDN_JS = {
    'integrity': "sha256-WUKHnLrIrx8dew//IpSEmPN/NT3DGAEmIePQYIEJLLs= sha384-53StQWuVbn6figscdDC3xV00aYCPEz3srBdV/QGSXw3f19og3Tq2wTRe0vJqRTEO sha512-X9O+2f1ty1rzBJOC8AXBnuNUdyJg0m8xMKmbt9I3Vu/UOWmSg5zG+dtnje4wAZrKtkopz/PEDClHZ1LXx5IeOw==",
    'url': "https://cdn.jsdelivr.net/npm/foundation-sites@6.5.1/dist/js/foundation.min.js",
}

JQUERY_URL = "http://code.jquery.com/jquery-%(version)s.min.js"
JQUERY_URLS = {
    1: "sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=",
    2: "ha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=",
    3: "sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
}

JQUERY_MOBILE_URL = "http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"

JQUERY_UI = {
    'integrity': "sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=",
    'url': "http://code.jquery.com/ui/1.12.1/jquery-ui.min.js",
}

# <script src="http://code.jquery.com/color/jquery.color-2.1.2.min.js"
# integrity="sha256-H28SdxWrZ387Ldn0qogCzFiUDDxfPiNIyJX7BECQkDE=" crossorigin="anonymous"></script>

UIKIT3 = "uikit3"

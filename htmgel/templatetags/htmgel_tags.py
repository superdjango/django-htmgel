# Imports

from copy import copy

from django import template
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.forms.utils import flatatt
from django.utils.safestring import mark_safe
from hashlib import md5
import os
from ..constants import *
from ..library import JavaScriptCollection
from ..shortcuts import parse_template
from ..utils import get_html_framework, get_input_type, is_required_field

register = template.Library()

# Exports

__all__ = (
    "attr_of",
    "css_url",
    "flatten",
    "gravatar",
    "icon",
    "index_of",
    "input_type",
    "is_required",
    "jquery_url",
    "js_url",
    "replace",
)

# Constants

if hasattr(settings, "HTMGEL_FRAMEWORK"):
    HTML_FRAMEWORK = getattr(settings, "HTMGEL_FRAMEWORK")
else:
    HTML_FRAMEWORK = get_html_framework()

ICON_FRAMEWORK = getattr(settings, "HTMGEL_ICONS", "fontawesome")

# Tags


@register.simple_tag()
def css_url(local=False, theme=None):
    attributes = dict()

    if HTML_FRAMEWORK == BOOTSTRAP4:
        if local and theme is not None:
            raise NotImplementedError("Using Bootswatch theme locally is not yet supported.")

        if theme is not None:
            url = BOOTSWATCH_THEMES % {'theme': theme}

            attributes['integrity'] = BOOTSWATCH_THEMES[theme]
            attributes['crossorigin'] = "anonymous"
        elif local:
            url = "%sbundled/bootstrap4/css/bootstrap.min.css" % settings.STATIC_URL
        else:
            url = BOOTSTRAP4_CDN_CSS['url']
            attributes['integrity'] = BOOTSTRAP4_CDN_CSS['integrity']
            attributes['crossorigin'] = "anonymous"
    elif HTML_FRAMEWORK == FOUNDATION6:
        if local:
            url = "%sbundled/foundation6/css/foundation.min.css" % settings.STATIC_URL
        else:
            url = FOUNDATION6_CDN_CSS['url']
            attributes['integrity'] = FOUNDATION6_CDN_CSS['integrity']
            attributes['crossorigin'] = "anonymous"
    elif HTML_FRAMEWORK == UIKIT3:
        url = "%sbundled/uikit3/css/uikit.min.css" % settings.STATIC_URL
    elif HTML_FRAMEWORK == "custom":
        url = "%slocal/css/custom.css"
    else:
        raise ImproperlyConfigured("Unrecognized or unsupported HTML/CSS framework: %s" % HTML_FRAMEWORK)

    return mark_safe('<link href="%s" rel="stylesheet" %s>' % (url, flatatt(attributes)))


@register.filter
def flatten(attributes):
    return flatatt(attributes)


@register.simple_tag
def gravatar(user, size=60):
    """Get the Gravatar for the given user."""
    encoded_email = user.email.strip().lower().encode("utf-8")
    email_hash = md5(encoded_email).hexdigest()

    return "https://secure.gravatar.com/avatar/%s.jpg?s=%s" % (email_hash, size)


@register.simple_tag(takes_context=True)
def html(context, path, **kwargs):
    if ".html" not in path:
        path += ".html"

    template_path = os.path.join("htmgel", "_frameworks", HTML_FRAMEWORK, path)

    # print("PATH =", template_path)
    # print("CONTEXT =", context)
    # print("-" * 80)

    # The context argument is a RequestContext which is *not* a dictionary, but a contains a list of dictionaries that
    # is iterable. To pass the context, we need to process each dictionary.
    _context = dict()
    for d in context:
        _context.update(d)

    # Add keyword arguments passed to the tag. These are specific to the template being loaded.
    _context.update(kwargs)

    # _context = {
    #     'current_page': context.get("current_page", None),
    #     'is_paginated': context.get("is_paginated", None),
    #     'page_keyword': context.get("page_keyword", "page"),
    #     'pagination_style': context.get("pagination_style", "previous-next"),
    #     'paginator': context.get("paginator", None),
    #     'request': context['request'],
    #     'subtitle': context.get("subtitle", None),
    #     'title': context.get("title", None),
    #     'verbose_name': context.get("verbose_name", None),
    #     'verbose_name_plural': context.get("verbose_name_plural", None),
    # }
    # _context.update(kwargs)

    return parse_template(template_path, _context)


@register.simple_tag
def jquery_url(local=False, major=2, mobile=False, ui=False):
    js = JavaScriptCollection()

    if major == 1:
        version = "1.12.4"
    elif major == 2:
        version = "2.2.4"
    elif major == 3:
        version = "3.3.1"
    else:
        version = None

    if version is not None:
        if local:
            url = "%sbundled/jquery/%s/js/jquery.min.js" % (settings.STATIC_URL, version)
            js.append("jquery", url=url)
        else:
            attributes = {
                'integrity': JQUERY_URLS[major],
                'crossorigin': "anonymous"
            }
            url = JQUERY_URL % {'version': version}
            js.append("jquery", url=url, **attributes)

    a = list()
    for s in js:
        a.append(s.to_html())

    return mark_safe("\n".join(a))


@register.simple_tag()
def js_url(local=False):
    attributes = dict()

    if HTML_FRAMEWORK == BOOTSTRAP4:
        if local:
            url = "%sbundled/bootstrap4/js/bootstrap.min.js" % settings.STATIC_URL
        else:
            url = BOOTSTRAP4_CDN_JS['url']
            attributes['integrity'] = BOOTSTRAP4_CDN_JS['integrity']
            attributes['crossorigin'] = "anonymous"
    elif HTML_FRAMEWORK == FOUNDATION6:
        if local:
            url = "%sbundled/foundation6/js/foundation.min.js" % settings.STATIC_URL
        else:
            url = FOUNDATION6_CDN_JS['url']
            attributes['integrity'] = FOUNDATION6_CDN_JS['integrity']
            attributes['crossorigin'] = "anonymous"
    elif HTML_FRAMEWORK == UIKIT3:
        # TODO: Incorporate UI KIt icons js?
        url = "%sbundled/uikit3/js/uikit.min.js" % settings.STATIC_URL
    elif HTML_FRAMEWORK == "custom":
        url = "%slocal/js/custom.js"
    else:
        raise ImproperlyConfigured("Unrecognized or unsupported HTML/CSS framework: %s" % HTML_FRAMEWORK)

    return mark_safe('<script src="%s" %s></script>' % (url, flatatt(attributes)))


@register.filter
def attr_of(instance, name):
    """Get the value of an attribute from a given instance.

    :param instance: The instance.
    :type instance: object

    :param name: The attribute name.
    :type name: str

    """
    return getattr(instance, name)


@register.simple_tag()
def icon(name, framework=ICON_FRAMEWORK):
    """Output an icon.

    :param name: The name of the icon.
    :type name: str

    :param framework: The icon framework to use.
    :type framework: str

    :rtype: str

    """
    # IDEA: It would be a LOT of work, but we *could* validate the given icon name against the selected framework. If
    # the icon doesn't exist, we could raise an exception, log an error, or return a default.

    if framework in ("fa", "fontawesome"):
        return mark_safe('<i class="fa fa-%s" aria-hidden="true"></i>' % name)
    elif framework in ("glyph", "glyphicon"):
        return mark_safe('<span class="glyphicon glyphicon-%s" aria-hidden="true"></span>' % name)
    else:
        raise ImproperlyConfigured("Unrecognized ICON_FRAMEWORK: %s" % framework)


@register.filter
def index_of(obj, index):
    """Get the value from the given index.

    :param obj: A list or tuple.
    :type obj: list | tuple

    :param index: The index to return.
    :type index: int

    """
    try:
        return obj[index]
    except IndexError:
        if settings.DEBUG:
            raise


@register.filter
def input_type(field):
    """Get the type of field (in lower case)."""
    return get_input_type(field).lower()


@register.filter
def is_required(field):
    """Indicates whether a form field is required."""
    return is_required_field(field)


@register.filter
def replace(text, from_string, to_string):
    """Replace a string."""
    return text.replace(from_string, to_string)


@register.simple_tag(takes_context=True)
def url_query_string(context, skip=None):
    """Return the request query string."""
    if "request" not in context:
        return ""

    request = context['request']

    _skip = list()
    if skip is not None:
        for i in skip.split(","):
            _skip.append(i.strip())

    url = list()
    for key, value in request.GET.items():
        if key in _skip:
            continue

        url.append("%s=%s" % (key, value))

    if len(url) == 0:
        return ""
    elif len(url) == 1:
        return "&" + "".join(url)
    else:
        return "&" + "&".join(url)

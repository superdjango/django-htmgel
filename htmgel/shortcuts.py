"""
Shortcuts, especially for working with Django templates in an ad-hoc mannner.

"""

# Imports

from django.template import Context, loader, Template, TemplateDoesNotExist

# Exports

__all__ = (
    "parse_string",
    "parse_template",
    "template_exists",
)

# Functions


def parse_string(string, context):
    """Parse a string as a Django template.

    :param string: The name of the template.
    :type string: str

    :param context: Context variables.
    :type context: dict

    :rtype: str

    """
    template = Template(string)
    return template.render(Context(context))


def parse_template(template, context):
    """Ad hoc means of parsing a template using Django's built-in loader.

    :param template: The name of the template.
    :type template: str || unicode

    :param context: Context variables.
    :type context: dict

    :rtype: str

    """
    return loader.render_to_string(template, context)


def template_exists(name):
    """Indicates whether the given template exists.

    :param name: The name (path) of the template to load.
    :type name: str

    :rtype: bool

    """
    try:
        loader.get_template(name)
        return True
    except TemplateDoesNotExist:
        return False
